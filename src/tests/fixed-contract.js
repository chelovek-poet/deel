import ContractsPage from 'src/pageobjects/contracts.page';
import FixedContractPage from 'src/pageobjects/fixed-contract.page';
import allure from '@wdio/allure-reporter';

describe("Contract creation tests", () => {
  it("Create Fixed Contract", () => {
    const description = `<ul>
    <li>Open Fixed Contract creation form</li>
    <li>Fill the necessary fields</li>
    <li>Submit form</li>
    <li>Check that review&sign page opened</li>
    </ul>`;
    allure.addDescription(description, 'html');

    // arrange
    // someLoginHelperFunction();
    ContractsPage.open();

    // action
    ContractsPage.clickFixedRate();

    FixedContractPage.inputContractName('Sergey Aksyonov QA offer');
    FixedContractPage.selectContractorsTaxResidence('united states');
    FixedContractPage.selectState('colorado');
    FixedContractPage.inputJobTitle('software qa', 0);
    FixedContractPage.selectSeniorityLevel('senior');
    FixedContractPage.fillScopeOfWork('1. Rule the world\n2. Sleep');
    FixedContractPage.setContractorsStartDateInPast(1);
    FixedContractPage.clickNext();

    FixedContractPage.selectCurrency('usd');
    FixedContractPage.inputPaymentRate(1000);
    FixedContractPage.selectPaymentFrequency('weekly');
    FixedContractPage.clickNext();

    FixedContractPage.clickNext();

    FixedContractPage.addSpecialClause('personal helicopter');
    FixedContractPage.clickNext();
    FixedContractPage.clickCreateContract();

    // assert
    FixedContractPage.checkReviewSignAvailable();
  });
});