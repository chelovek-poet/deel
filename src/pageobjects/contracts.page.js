import allure from '@wdio/allure-reporter';

class ContractTypePage {
  // Elements
  //--------------------------------------------------------------------------------------------------------------------
  get fixedRateButton() {
    return $('.heap-start-fixed-contract div');
  }
  
  // Actions
  //--------------------------------------------------------------------------------------------------------------------
  @step
  open() {
    browser.url("/create");
  }

  @step()
  clickFixedRate() {
    this.fixedRateButton.WaitForClickable();
    this.fixedRateButton.click();
  }
}

export default new ContractTypePage();