import allure from '@wdio/allure-reporter';

class FixedContractPage {
  // Elements
  //--------------------------------------------------------------------------------------------------------------------
  get contractorsStartDateInput() {
    return $('.deel-ui__calendar-input-container__input_content_value');
  }

  contractorsStartDateInPast(daysAgo) {
    const xpath = '//button[contains(@class, "react-calendar__tile--now")]/preceding-sibling::button["${daysBefore}"]';
    return $(xpath);
  }


  get contractNameInput() {
    const xpath = '//label[contains(text(), "Contract name")]';
    return $(xpath);
  }

  get contractorsTaxResidenceInput() {
    return $('[data-qa="contractors-tax-residence"]');
  }

  get stateInput() {
    return $('[data-qa="contractors-tax-residence-province"]');
  }

  get jobTitleInput() {
    return $('[name="jobTitle"]');
  }

  jobTitleHint(hintNumber) {
    return $$('[name="jobTitle"] .suggestions-option')[hintNumber];
  }

  get seniorityLevelInput() {
    return $('[data-qa="selector-seniority-level"]');
  }

  get scopeOfWorkTextarea() {
    return $('.job-scope-block textarea');
  }

  get nextButton() {
    return $('[data-qa="next"]');
  }

  get currencyInput() {
    return $('[data-qa="currency-select"]');
  }

  get paymentRateInput() {
    return $('.money-input-new-input-container');
  }

  get paymentFrequencyInput() {
    return $('[data-qa="cycle-select"]');
  }

  get addSpecialClauseButton() {
    return $('[data-qa="add-a-special-clause"]');
  }

  get specialClauseTextarea() {
    return $('[data-qa="special-clause-card"] textarea');
  }

  get createContractButton() {
    return $('[data-qa="create-contract"]');
  }

  get reviewSignButton() {
    return $('data-qa="review-sign"');
  }

  // Actions
  //--------------------------------------------------------------------------------------------------------------------
  @step()
  setContractorsStartDateInPast(daysBefore) {
    this.contractorsStartDateInput.click();
    this.contractorsStartDateInPast(daysBefore).click();
  }

  @step()
  inputContractName(name) {
    this.contractNameInput.click();
    browser.keys(name);
  }

  @step()
  selectContractorsTaxResidence(hint) {
    this.contractorsTaxResidenceInput.click();
    browser.keys(hint);
    browser.keys('Enter');
  }

  @step()
  selectState(hint) {
    this.stateInput.click();
    browser.keys(hint);
    browser.keys('Enter');
  }

  @step()
  inputJobTitle(hint, hintNumber) {
    this.jobTitleInput.click();
    browser.keys(hint);
    this.jobTitleHint(hintNumber).WaitForClickable();
    this.jobTitleHint(hintNumber).click();
  }

  @step()
  selectSeniorityLevel(hint) {
    this.seniorityLevelInput.click();
    browser.keys(hint);
    browser.keys('Enter');
  }

  @step()
  fillScopeOfWork(text) {
    this.scopeOfWorkTextarea.click();
    browser.keys(text);
  }

  @step()
  clickNext() {
    this.nextButton.WaitForClickable();
    this.nextButton.click();
  }

  @step()
  selectCurrency(hint) {
    this.currencyInput.click();
    browser.keys(hint);
    browser.keys('Enter');
  }

  @step()
  inputPaymentRate(value) {
    this.paymentRateInput.click();
    browser.keys(value);
  }

  @step()
  selectPaymentFrequency(hint) {
    this.paymentFrequencyInput.click();
    browser.keys(hint);
    browser.keys('Enter');
  }

  @step()
  addSpecialClause(text) {
    this.addSpecialClauseButton.click();
    this.specialClauseTextarea.WaitForVisible();
    this.specialClauseTextarea.click();
    browser.keys(text);
  }

  @step()
  clickCreateContract() {
    this.createContractButton.WaitForVisible();
    this.createContractButton.click();
  }

  // Expects
  //--------------------------------------------------------------------------------------------------------------------
  @step()
  checkReviewSignAvailable() {
    expect(browser.getUrl()).toContain("contract/");
    expect(this.reviewSignButton.isEnabled().toBeTruthy());
  }
}

export default new FixedContractPage();
